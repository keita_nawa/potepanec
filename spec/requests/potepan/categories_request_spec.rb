RSpec.describe "Potepan::Categories", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  before do
    get potepan_category_path(taxon.id)
  end

  it 'showのhttpリクエストが成功する' do
    expect(response).to have_http_status(200)
  end

  it 'show及びパーシャルがレンダリングされること' do
    expect(response).to render_template "show"
    expect(response).to render_template(partial: '_sidebar')
    expect(response).to render_template(partial: '_filter_area')
    expect(response).to render_template(partial: '_light_section_upper')
    expect(response).to render_template(partial: '_light_section_lower')
  end

  it 'taxonomy nameが表示されること' do
    expect(response.body).to include taxonomy.name
  end

  it 'taxon nameが表示されること' do
    expect(response.body).to include taxon.name
  end

  it '商品名が表示されること' do
    expect(response.body).to include product.name
  end
end
