RSpec.describe 'Potepan::products', type: :request do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }

  before do
    get potepan_product_path(product.id)
  end

  it 'showのhttpリクエストが成功する' do
    expect(response).to have_http_status(200)
  end

  it 'show及びパーシャルがレンダリングされること' do
    expect(response).to render_template "show"
    expect(response).to render_template(partial: '_light_section')
    expect(response).to render_template(partial: '_related_products')
  end

  it '商品名が表示されること' do
    expect(response.body).to include product.name
  end

  it '商品の値段が表示されること' do
    expect(response.body).to include product.display_price.to_s
  end

  it '商品の説明が表示されること' do
    expect(response.body).to include product.description
  end
end
