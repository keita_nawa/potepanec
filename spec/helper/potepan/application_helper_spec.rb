RSpec.describe "ApplicationHelperTest", type: :helper do
  include ApplicationHelper

  describe "title" do
    it "page_title の値が存在する" do
      expect(title("item1")).to eq "item1 - #{Const::BASE_TITLE}"
    end

    it "page_title の値が空である" do
      expect(title("")).to eq Const::BASE_TITLE
    end

    it "page_title の値がnilである" do
      expect(title(nil)).to eq Const::BASE_TITLE
    end
  end
end
