RSpec.describe "categories/show.html.erb", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, parent: taxonomy.root) }
  let!(:product) { create(:product, taxons: [taxon]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  it '適切なカテゴリタイトルが表示される' do
    expect(page).to have_title "#{taxon.name} - #{Const::BASE_TITLE}"
  end

  it '適切なカテゴリー名が表示される' do
    within '.pageHeader' do
      expect(page).to have_selector 'h2', text: taxon.name
      expect(page).to have_selector 'li', text: taxon.name
    end
  end

  it '商品名、商品価格が表示される' do
    within '.productBox' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
    end
  end

  it '商品をクリックすると意図したページに遷移する' do
    within page.first '.productBox' do
      expect(page).to have_link product.name
      click_link product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end
  end

  it 'サイドバーで適切なカテゴリー名、個数が表示される' do
    within page.first '.side-nav' do
      expect(page).to have_content taxonomy.name
      expect(page).to have_link taxon.name
      expect(page).to have_content taxon.products.size
      click_link taxon.name
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end
end
