require 'spree/testing_support/factories'

RSpec.feature 'products/show.html.erb', type: :feature do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  it '各商品毎にタイトル、商品名、値段、説明が表示される' do
    expect(page).to have_title "#{product.name} - #{Const::BASE_TITLE}"
    within '.media-body' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end
  end

  it '一覧ページへ戻るボタンから正常に遷移する' do
    expect(page).to have_link "一覧ページへ戻る"
    click_link "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(taxon.id)
  end

  it '1つ目のhomeボタンから正常に遷移する' do
    within '.collapse' do
      expect(page).to have_link "Home"
      click_link "Home"
      expect(current_path).to eq potepan_path
    end
  end

  it '2つ目のhomeボタンから正常に遷移する' do
    within '.lightSection' do
      expect(page).to have_link "Home"
      click_link "Home"
      expect(current_path).to eq potepan_path
    end
  end
end
